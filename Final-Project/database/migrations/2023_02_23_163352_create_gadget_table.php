<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGadgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadget', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->text('spesifikasi');
            $table->string('image');
            $table->string('tahun');

            $table->unsignedBigInteger('merk_id');
            $table->foreign('merk_id')->references('id')->on('merk')->onDelete('cascade');
            
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('type')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gadget');
    }
}
