<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\GadgetController;
use App\Http\Controllers\MerkController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.main-test');
});

// CRUD GADGET //
Route::resource('gadget', GadgetController::class);



Route::middleware(['auth'])->group(function () {
    // CRUD MERK //
Route::resource('merk', MerkController::class);

// CRUD PROFILE //
Route::resource('profile', ProfileController::class);

// CRUD REVIEW //
Route::resource('review', ReviewController::class);

// CRUD TYPE //
Route::resource('type', TypeController::class);
// CRUD GADGET //
Route::resource('users', UsersController::class);
;
});


//Route auth

Auth::routes();

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');