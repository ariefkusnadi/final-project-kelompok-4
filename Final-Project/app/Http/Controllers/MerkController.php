<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\merk;

class MerkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merk = merk::all();
        return view('merk.tampil', ['merk'=>$merk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('merk.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
        'nama' => 'required'
       ]);

//insert ke database
       $merk = new merk;

       $merk->nama = $request->nama;

       $merk->save();

       return redirect('/merk')->with('success', 'Data Merk Berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $merk = merk::find($id);

        return view('merk.detail', ['merk'=>$merk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merk = merk::find($id);
        return view('merk.edit', ['merk'=>$merk]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
           ]);

        $merk = merk::find($id);

       $merk->nama = $request->nama;

       $merk->save();

       return redirect('/merk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $merk = merk::find($id);
        $merk->delete();

        return redirect('/merk');
    }
}
