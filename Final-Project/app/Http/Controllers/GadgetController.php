<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gadget;
use App\Models\merk;
use App\Models\Type;
use File;

class GadgetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gadget = Gadget::all();
        return view('gadget.tampil', ['gadget'=>$gadget]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merk = merk::all();
        $type = Type::all();
        return view('gadget.tambah', ['merk' => $merk], ['type' => $type]);
    }

    public function review($id)
    {
    
        $review = Review::all();
        $gadget = Gadget::Find($id);

        return view('gadget.review', ['review'=>$review, 'gadget' => $gadget]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'spesifikasi' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max::2048',
            'tahun' => 'required',
            'merk_id' => 'required',
            'type_id' => 'required',
           ]);
           
//konversi nama gambar
           $imageName = time().'.'.$request->image->extension();

//gambar masuk ke folder public/image
           $request->image->move(public_path('image'), $imageName);
    
    //insert ke database
           $gadget = new Gadget;
    
           $gadget->nama = $request->nama;
           $gadget->spesifikasi = $request->spesifikasi;
           $gadget->image = $imageName;
           $gadget->tahun = $request->tahun;
           $gadget->merk_id = $request->merk_id;
           $gadget->type_id = $request->type_id;
    
           $gadget->save();
    
           return redirect('/gadget')->with('success', 'Data Gadget Berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gadget = Gadget::Find($id);

        return view('gadget.detail', ['gadget'=>$gadget]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $merk = merk::all();
        $type = Type::all();
        $gadget = Gadget::Find($id);

        return view('gadget.edit', ['gadget'=>$gadget, 'merk' => $merk, 'type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'spesifikasi' => 'required',
            'image' => 'mimes:png,jpg,jpeg|max::2048',
            'tahun' => 'required',
            'merk_id' => 'required',
            'type_id' => 'required',
           ]);

           $gadget = Gadget::find($id);

           $gadget->nama = $request->nama;
           $gadget->spesifikasi = $request->spesifikasi;
           $gadget->tahun = $request->tahun;
           $gadget->merk_id = $request->merk_id;
           $gadget->type_id = $request->type_id;

           if ($request->has('image')) {
            $path = 'image/';
            File::delete($path. $gadget->image);

            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('image'), $imageName);

            $gadget->image = $imageName;

           }

           $gadget->save();

           return redirect('/gadget');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gadget = Gadget::find($id);

        $path = 'image/';
            File::delete($path. $gadget->image);
            
        $gadget->delete();

        return redirect('/gadget');
    }
}
