<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gadget extends Model
{
    protected $table = 'gadget';
    protected $fillable = ['nama', 'spesifikasi', 'image', 'tahun', 'merk_id','type_id'];
    use HasFactory;

    public function merk()
    {
        return $this->belongsTo(Gadget::class, 'merk_id');
    }

    public function review()
    {
        return $this->hasMany(Review::class, 'gadget_id');
    }

}
