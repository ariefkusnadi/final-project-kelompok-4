<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class merk extends Model
{
    protected $table = 'merk';
    protected $fillable = ['nama'];
    use HasFactory;

    public function gadget()
    {
        return $this->hasMany(Gadget::class, 'merk_id');
    }

}
