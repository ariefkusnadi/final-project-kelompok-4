<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'review';
    protected $fillable = ['user_id', 'gadget_id', 'content'];
    use HasFactory;

    public function gadget()
    {
        return $this->belongsTo(Gadget::class, 'gadget_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
