@extends('layouts.main')
@section('title')
    Halaman Tampil Gadget
@endsection

@section('content')


        <div class="card">
            <img src="{{asset('/image/' . $gadget->image)}}"  class="card-img-top">
            <div class="card-body">
              <h1>{{$gadget->nama}}</h1>
              <p class="card-text">{{$gadget->spesifikasi}}</p>
              

            <a href="/gadget" class="btn btn-secondary btn-sm">Kembali</a>
            {{-- <a href="/gadget/{{$gadget->id}}/review" class="btn btn-primary btn-sm">Add Review</a> --}}
          </div>
        </div>
            
@endsection