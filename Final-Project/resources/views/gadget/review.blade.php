@extends('layouts.main')
@section('title')
    Halaman Edit Gadget
@endsection

@section('content')
<form action="/gadget/{{$gadget->id}}"  method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$gadget->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>Spesifikasi</label>
      <textarea name="spesifikasi" class="form-control" cols="30" rows="10">{{$gadget->spesifikasi}}</textarea>
    </div>
    @error('spesifikasi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Image</label>
    <input type="file" name="image" class="form-control">
  </div>
  @error('image')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Tahun</label>
    <input type="string" name="tahun" value="{{$gadget->tahun}}" class="form-control">
  </div>
  @error('tahun')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Merk</label>
    <select name="merk_id" class="form-control" id="">
        <option value="">---Silahkan Pilih Merk---</option>
        @forelse ($merk as $item)
        @if ($item->id === $gadget->merk_id)

        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
        @else
        <option value="{{$item->id}}">{{$item->nama}}</option>
        @endif
            
        @empty
        <option value="">Belum ada Merk</option>
        @endforelse
    </select>
</div>
  @error('merk_id')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Type</label>
    <select name="type_id" class="form-control" id="">
        <option value="">---Silahkan Pilih Type---</option>
        @forelse ($type as $item)
        @if ($item->id === $gadget->type_id)

        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
        @else
        <option value="{{$item->id}}">{{$item->nama}}</option>
        @endif

        @empty
        <option value="">Belum ada Type</option>
        @endforelse
    </select>
</div>
  @error('type_id')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    
<button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection