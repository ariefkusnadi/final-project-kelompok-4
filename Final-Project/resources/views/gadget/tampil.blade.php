@extends('layouts.main')
@section('title')
    Halaman Tampil Gadget
@endsection

@section('content')

@auth
<a href="/gadget/create" class="btn btn-primary my-2">Tambah</a>
@endauth


<div class="row">
    @forelse ($gadget as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('/image/' . $item->image)}}" height="250px"  class="card-img-top">
            <div class="card-body">
              <h1>{{$item->nama}}</h1>
              <p class="card-text">{{Str::limit($item->spesifikasi,30)}}</p>
              <a href="/gadget/{{$item->id}}" class="btn btn-info btn-block">Detail</a>
             
              @auth
              <div class="row mt-3">
                <div class="col-6">
                    <a href="/gadget/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
                </div>
                <div class="col-6">
                    <form action="/gadget/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-block" value="Delete">
                    </form>
                </div>
              </div> 
              @endauth
              
            </div>
        </div>
    </div>
    @empty
        <h1>Tidak Ada Gadget</h1>
    @endforelse

    

</div>
@include('sweetalert::alert')
@endsection