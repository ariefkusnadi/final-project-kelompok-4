@extends('layouts.main')
@section('title')
    Halaman Tambah Merk
@endsection

@section('content')
<form action="/merk" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Merk</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection