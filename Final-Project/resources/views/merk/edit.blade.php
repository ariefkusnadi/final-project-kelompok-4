@extends('layouts.main')
@section('title')
    Halaman Edit Merk
@endsection

@section('content')
<form action="/merk/{{$merk->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Merk</label>
      <input type="text" name="nama" value="{{$merk->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection