@extends('layouts.main')
@section('title')
    Halaman Tampil Merk
@endsection

@section('content')

<div class="card my-3">
        <div class="card-header">
          <h1>Merk</h1>
        </div>
        <div class="card-body">
          <h5 class="card-title">{{$merk->nama}}</h5>
        </div>
      
      <div class="row">
      @forelse ($merk->gadget as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('/image/' . $item->image)}}" height="250px"  class="card-img-top">
                <div class="card-body">
                  <h1>{{$item->nama}}</h1>
                  <p class="card-text">{{Str::limit($item->spesifikasi,30)}}</p>
                  <a href="/gadget/{{$item->id}}" class="btn btn-info btn-block">Detail</a>
                </div>
            </div>
        </div>
      @empty
      
      @endforelse
</div>
    <a href="/merk" class="btn btn-secondary btn-sm">Kembali</a>




@endsection