@extends('layouts.main')
@section('title')
    Halaman Tampil Merk
@endsection

@section('content')

<a href="/merk/create" class="btn btn-primary my-2">Tambah</a>


    @forelse ($merk as $item)
    <div class="card my-3">
        <div class="card-header">
          <h1>Merk</h1>
        </div>
        <div class="card-body">
          <h5 class="card-title">{{$item->nama}}</h5>
          <a href="/merk/{{$item->id}}" class="btn btn-primary">Read More</a>
          <a href="/merk/{{$item->id}}/edit" class="btn btn-primary">Edit</a>

          <form action="/merk/{{$item->id}}" method="post">
            @csrf
            @method('delete')
            <input type="submit" class="btn btn-danger btn-primary my-3" value="Delete">
          </form>
        </div>
      </div>
    
      @empty
        <h1>Tidak Ada Merk</h1>
    @endforelse

    </div>
</div>
@include('sweetalert::alert')


@endsection