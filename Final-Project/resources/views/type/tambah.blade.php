@extends('layouts.main')
@section('title')
    Halaman Tambah Type
@endsection

@section('content')
<form action="/type" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Type</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection