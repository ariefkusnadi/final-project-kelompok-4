@extends('layouts.main')
@section('title')
    Halaman Edit Type
@endsection

@section('content')
<form action="/type/{{$type->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Type</label>
      <input type="text" name="nama" value="{{$type->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection