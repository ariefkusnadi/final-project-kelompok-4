@extends('layouts.main')
@section('title')
    Halaman Tampil Type
@endsection

@section('content')

    <div class="card my-3">
        <div class="card-header">
          <h1>Type</h1>
        </div>
        <div class="card-body">
          <h5 class="card-title">{{$type->nama}}</h5>
        </div>
      </div>
    

    </div>
    <a href="/type" class="btn btn-secondary btn-sm">Kembali</a>
</div>



@endsection