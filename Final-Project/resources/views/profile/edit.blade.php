@extends('layouts.main')
@section('title')
    Halaman Edit Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}"  method="POST">
    @csrf
    @method('put')

    <div class="form-group">
      <input type="text" name="nama" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>

    <div class="form-group">
      <label>Nama</label>
      <input type="name" name="nama" class="form-control" value="{{$profile->nama}}">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Umur</label>
    <input type="number" name="umur" class="form-control" value="{{$profile->umur}}">
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

<div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
  </div>
  @error('alamat')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection