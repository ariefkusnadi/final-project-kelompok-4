<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Final Project Kelompok 4</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icon -->
      <link rel="icon" href="{{asset('template/images/fevicon.png') }}" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="{{asset('template/css/bootstrap.min.css') }}" />
      <!-- site css -->
      <link rel="stylesheet" href="{{asset('template/style.css') }}" />
      <!-- responsive css -->
      <link rel="stylesheet" href="{{asset('template/css/responsive.css') }}" />
      <!-- color css -->
      <link rel="stylesheet" href="{{asset('templatecss/colors.css') }}" />
      <!-- select bootstrap -->
      <link rel="stylesheet" href="{{asset('template/css/bootstrap-select.css') }}" />
      <!-- scrollbar css -->
      <link rel="stylesheet" href="{{asset('template/css/perfect-scrollbar.css') }}" />
      <!-- custom css -->
      <link rel="stylesheet" href="{{asset('template/css/custom.css') }}" />
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

      <script src="https://cdn.tiny.cloud/1/46df7gyg6tx21m2ezgyej1jh0hd2anyv1b964h9tc1x37d6o/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

   </head>
   <body class="dashboard dashboard_1">
      <div class="full_container">
         <div class="inner_container">
            <!-- Sidebar  -->
               @include('layouts.main-sidebar')
            <!-- end sidebar -->
            <!-- right content -->
            <div id="content">
               <!-- topbar -->
               @include('layouts.main-nav')
               <!-- end topbar -->
               <!-- dashboard inner -->
               <div class="midde_cont">
                  <div class="container-fluid">
                     <div class="row column_title">
                        <div class="col-md-12">
                           <div class="page_title">
                              <h2>@yield('title')</h2>
                           </div>
                        </div>
                     </div>

                     <div class="container-fluid">
                        <div class="white_shd p-5">
                           @yield('content')
                        </div>
                     </div>
                     
                  
                       

               
                  <!-- footer -->
                  @include('layouts.main-footer')
               </div>
               <!-- end dashboard inner -->
            </div>
         </div>
      </div>
      <!-- jQuery -->
      <script src="{{asset('template/js/jquery.min.js') }}"></script>
      <script src="{{asset('template/js/popper.min.js') }}"></script>
      <script src="{{asset('template/js/bootstrap.min.js') }}"></script>
      <!-- wow animation -->
      <script src="{{asset('template/js/animate.js') }}"></script>
      <!-- select country -->
      <script src="{{asset('/templatejs/bootstrap-select.js') }}"></script>
      <!-- owl carousel -->
      <script src="{{asset('template/js/owl.carousel.js') }}"></script> 
      <!-- chart js -->
      <script src="{{asset('template/js/Chart.min.js') }}"></script>
      <script src="{{asset('template/js/Chart.bundle.min.js') }}"></script>
      <script src="{{asset('template/js/utils.js') }}"></script>
      <script src="{{asset('template/js/analyser.js') }}"></script>
      <!-- nice scrollbar -->
      <script src="{{asset('template/js/perfect-scrollbar.min.js') }}"></script>
      <script>
         var ps = new PerfectScrollbar('#sidebar');
      </script>
      <!-- custom js -->
      <script src="{{asset('template/js/custom.js') }}"></script>
      <script src="{{asset('template/js/chart_custom_style1.js') }}"></script>

      @include('sweetalert::alert')

      <script>
         tinymce.init({
           selector: 'textarea',
           plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
           toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
           tinycomments_mode: 'embedded',
           tinycomments_author: 'Author name',
           mergetags_list: [
             { value: 'First.Name', title: 'First Name' },
             { value: 'Email', title: 'Email' },
           ]
         });
       </script>

   </body>
</html>