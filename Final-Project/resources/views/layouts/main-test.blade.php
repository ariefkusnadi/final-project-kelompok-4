@extends('layouts.main')
@section('title')
    Final Project Kelompok 4
@endsection

@section('content')
<p> Pada tugas akhir pembuatan website ini, Kami kelompok 4 telah berhasil menyelesaikan tugas dengan baik dan menghasilkan sebuah website yang memenuhi kebutuhan tugas. 
    Proses pembuatan dimulai dengan analisis kebutuhan dan pengumpulan informasi mengenai konten dan fitur yang diinginkan. 
    Selanjutnya, tim melakukan desain tampilan website dengan mempertimbangkan keterbacaan, kejelasan, dan keteraturan tampilan. Setelah itu, pengembangan website dilakukan dengan memperhatikan aspek keamanan, 
    kecepatan akses, dan kemudahan penggunaan. Sebelum diluncurkan, website ini telah melalui proses uji coba dan perbaikan yang diperlukan. Dengan selesainya proyek ini, 
    diharapkan website yang dibuat dapat memberikan manfaat bagi klien dan pengguna yang mengaksesnya.</p>

<p> 
    Website yang dibuat memiliki desain yang menarik dan responsif di berbagai perangkat. 
    Selain itu, fitur-fitur yang diinginkan seperti fitur pencarian dan kategori, telah berhasil diimplementasikan dengan baik. 
    Hal ini membuat website mudah digunakan dan navigasi menjadi lebih lancar. Keamanan website juga menjadi prioritas selama proses pembuatan, 
    sehingga website ini telah diimplementasikan dengan sistem keamanan yang memadai untuk mencegah akses tidak sah atau serangan hacker. 
    Dengan hasil yang memuaskan, tim berharap website ini dapat memberikan manfaat dan pengguna yang mengaksesnya, serta dapat menjadi referensi bagi proyek-proyek pembuatan website selanjutnya.</p> 

{{-- <p>Fusce malesuada ante vitae semper pellentesque. Ut posuere magna quis nibh lacinia cursus. Maecenas nibh erat, eleifend pharetra laoreet vel, tempor ac risus. Nulla facilisi. Pellentesque iaculis varius cursus. Nam pulvinar vel ante et aliquet. Curabitur nibh ipsum, varius at ante non, iaculis tincidunt libero. Donec pulvinar aliquet dapibus.</p> 

<p>Vestibulum ex libero, sagittis ac tincidunt eget, imperdiet quis nisl. Nullam interdum fermentum felis nec tincidunt. Quisque et tortor id eros dapibus varius. In porttitor, felis a commodo laoreet, lacus tellus consectetur dolor, ac viverra est risus placerat sapien. Vivamus feugiat eget ligula tincidunt venenatis. Suspendisse quis felis ut neque dictum elementum ut et neque. Mauris commodo feugiat ipsum, a rhoncus purus finibus sed. Mauris ultricies vitae ante non pulvinar. Etiam ut elit ac eros ornare pretium ut at massa. Etiam at nisi lobortis nisi porttitor tempus. Quisque scelerisque metus eu ex tempor consequat. Fusce iaculis ligula a suscipit vulputate. Curabitur ut sem ullamcorper, finibus sem quis, sodales magna. Cras sagittis, diam sit amet vehicula dapibus, nibh est porta orci, eget fermentum metus urna vitae eros.</p>

<p>Sed tempus convallis lorem, ac lobortis diam aliquet id. Nam sit amet dui placerat, posuere orci auctor, egestas nisi. Pellentesque id diam fringilla, placerat nisl quis, elementum erat. Etiam at leo turpis. Nam feugiat eget ipsum id sagittis. Praesent malesuada blandit ex, eget euismod turpis accumsan a. Duis a sem libero.</p> --}}

@endsection