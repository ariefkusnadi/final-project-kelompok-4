           <!-- Sidebar  -->
           <nav id="sidebar">
            <div class="sidebar_blog_1">
               <div class="sidebar-header">
                  <div class="logo_section">
                     <a href="index.html"><img class="logo_icon img-responsive" src="{{asset('template/images/logo/logo_icon.png') }}" alt="#" /></a>
                  </div>
               </div>
               <div class="sidebar_user_info">
                  <div class="icon_setting"></div>
                  <div class="user_profle_side">
                     <div class="user_img"><img class="img-responsive" src="{{asset('template/images/layout_img/user_img.jpg') }}" alt="#" /></div>
                     <div class="user_info">
                        
                        @auth
                        <h6>{{Auth::user()->name}}</h6>
                        <p><span class="online_animation"></span> Online</p>
                        @endauth
                        
                        @guest
                        <h6>John David</h6>
                        <p><span class="online_animation"></span> Online</p>
                        @endguest
                       
                     </div>
                  </div>
               </div>
            </div>
            <div class="sidebar_blog_2">
               <h4>General</h4>
               <ul class="list-unstyled components">
                  <li class="active">
                     <a href="/"><i class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a>
                     <ul class="collapse list-unstyled" id="dashboard">
   
                     </ul>
                  </li>
                  <li><a href="/gadget"><i class="fa fa-clock-o orange_color"></i> <span>Gadget</span></a></li>
                  @auth
                  <li><a href="/merk"><i class="fa fa-briefcase blue1_color"></i> <span>Merk</span></a></li>
                  <li><a href="/type"><i class="fa fa-briefcase green_color"></i> <span>Type</span></a></li>
                  <li><a href="/profile"><i class="fa fa-user green_color"></i> <span>Profile</span></a></li>
                  @endauth
                  

                  @guest
                  <li><a href="/login"><i class="fa fa-paper-plane red_color"></i> <span>Login</span></a></li>
                  @endguest
                
                  
               </ul>
            </div>
         </nav>
         <!-- end sidebar -->